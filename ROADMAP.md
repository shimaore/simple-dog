Issues/todos
------------

- [x] Write inbound parser
- [x] Add inbound support for common/easy-to-implement `midi_event` types (note, ctrl, …)
- Start writing proper Node.js API (based on e.g. `node-midi` and/or `jazz-midi` and/or W3C MIDI)
- [x] Provide commands and messages to enumerate and modify port connections
- [x] Make sure it works under Alpine Linux (and Alpine on Raspberry)
- Add inbound command to add remote destination and/or automatically register new remote destinations using their IP and port; send packets out to all destinations (we won't try to establish one-on-one sessions)
- Support more than one MIDI port
- Convert this issues list into proper Gitlab or Gitea issues.
