// --------------- asoundlib portion -------------- //

#include <alsa/asoundlib.h>

#define ENV_NAME "NAME"
#define DEFAULT_NAME "Doggy"

int report (const char* msg, int err) {
  if (err<0) {
    fprintf(stderr,"! %s error: %s\n", msg, snd_strerror(err));
  }
  return err;
}

char const alsa_loc[] = "default"; // "default"

typedef struct app_data app_data;
typedef struct seq_port seq_port;

typedef struct seq_port_list seq_port_list;

typedef struct seq_port_list {
  seq_port* car;
  seq_port_list* cdr;
} seq_port_list;

typedef struct seq_data {
  app_data* app;
  snd_seq_t* handle;
  int client;
  int max_clients;
  int max_ports;
  int max_queues;
  int max_channels;

  // queue (queues?)
  int queue;
  snd_midi_event_t* output_parser;

  snd_midi_event_t* input_parser; // not clear this is specific to a queue

  // ports
  seq_port_list* ports;
} seq_data;

typedef struct seq_port {
  seq_data* S;
  int port;
} seq_port;

// create a new client
void open_client (seq_data* s)
{
  if( report("open",snd_seq_open(&s->handle, alsa_loc, SND_SEQ_OPEN_DUPLEX, SND_SEQ_NONBLOCK)) <0 ) {
    return;
  }

  char const* name = getenv(ENV_NAME);
  if(!name) { name = DEFAULT_NAME; }
  report("client name",snd_seq_set_client_name(s->handle,name));
  // report("nonblock",snd_seq_nonblock(s->handle,1));

  snd_seq_system_info_t *sysinfo;
  snd_seq_system_info_alloca(&sysinfo);
  if( report("system info",snd_seq_system_info(s->handle, sysinfo)) >= 0 ){
    s->max_clients = snd_seq_system_info_get_clients(sysinfo);
    s->max_ports = snd_seq_system_info_get_ports(sysinfo);
    s->max_queues = snd_seq_system_info_get_ports(sysinfo);
    s->max_channels = snd_seq_system_info_get_channels(sysinfo);
    // printf("Max client %d ports %d queues %d channels %d\n", s->max_clients, s->max_ports, s->max_queues, s->max_channels);
  }
  s->client = report("client id",snd_seq_client_id(s->handle));
  s->ports = NULL;
}

void create_queue(seq_data* s) {
  s->queue = report("alloc queue",snd_seq_alloc_queue(s->handle));
  // printf("queue %d\n",s->queue);

  snd_seq_queue_tempo_t *tempo;
  snd_seq_queue_tempo_alloca(&tempo);
  snd_seq_queue_tempo_set_tempo(tempo, 500000); // 120 BPM, beat in µs
  snd_seq_queue_tempo_set_ppq(tempo, 96); // 96 PPQ, pulse per quarter note
  report("set tempo",snd_seq_set_queue_tempo(s->handle, s->queue, tempo));
  // "PPQ must be set before the queue is started"

  report("start queue",snd_seq_start_queue(s->handle,s->queue,NULL));

  /*
  snd_seq_queue_info_t *queue_info;
  snd_seq_queue_info_alloca(&queue_info);
  report("queue info",snd_seq_get_queue_info(s->handle,s->queue,queue_info));
  */

  report("drain output", snd_seq_drain_output(s->handle));

  snd_midi_event_new(16384,&s->output_parser);
  snd_midi_event_reset_encode(s->output_parser);

  snd_midi_event_new(16384,&s->input_parser);
  snd_midi_event_reset_decode(s->input_parser);
}

// create a new port

void create_subscriptions(seq_data* S, seq_port* P);

typedef enum {
  PORT_DIRECTION_INPUT = 1,
  PORT_DIRECTION_OUTPUT = 2,
  PORT_DIRECTION_BOTH = 3,
} PortDirection;

int new_port(seq_data* S,const char* name,PortDirection direction)
{
  snd_seq_port_info_t* pinfo;

  snd_seq_port_info_alloca(&pinfo);
  snd_seq_port_info_set_name(pinfo, name);
  snd_seq_port_info_set_type(pinfo, SND_SEQ_PORT_TYPE_MIDI_GENERIC);
  int caps = 0;
  if(direction & PORT_DIRECTION_INPUT) {
    caps |=
    SND_SEQ_PORT_CAP_WRITE| // writable: input
    SND_SEQ_PORT_CAP_SUBS_WRITE; // accept write subscription
  }
  if(direction & PORT_DIRECTION_OUTPUT) {
    caps |=
    SND_SEQ_PORT_CAP_READ| // readable: output
    SND_SEQ_PORT_CAP_SUBS_READ; // accept read subscription
  }
  snd_seq_port_info_set_capability(pinfo,caps);

  /* Enable timestamping for events sent by external subscribers. */
  snd_seq_port_info_set_timestamping(pinfo, 1);
  snd_seq_port_info_set_timestamp_real(pinfo, 1);
  snd_seq_port_info_set_timestamp_queue(pinfo, S->queue);

  /* Based on snd_seq_create_simple_port */
  snd_seq_port_info_set_midi_channels(pinfo,16);
  snd_seq_port_info_set_midi_voices(pinfo,64);
  snd_seq_port_info_set_synth_voices(pinfo,0);

  if(report("create port",snd_seq_create_port(S->handle, pinfo)) < 0) {
    return -1;
  }

  seq_port* P = malloc(sizeof(seq_port));
  P->S = S;
  P->port = snd_seq_port_info_get_port(pinfo);
  // printf("port %s %d\n",name,P->port);

  seq_port_list* prev = S->ports;
  S->ports = malloc(sizeof(seq_port_list));
  S->ports->car = P;
  S->ports->cdr = prev;
  create_subscriptions(S,P);
  return P->port;
}

void create_subscriptions(seq_data* S, seq_port* P) {
  snd_seq_port_subscribe_t *sub;
  snd_seq_port_subscribe_alloca(&sub);

  snd_seq_port_subscribe_set_queue(sub, S->queue);
  snd_seq_port_subscribe_set_time_update(sub, 1);
  snd_seq_port_subscribe_set_time_real(sub, 1);

  snd_seq_addr_t sender, dest;

  dest.client = S->client;
  dest.port = P->port;
  snd_seq_port_subscribe_set_dest(sub, &dest);

  sender.client = SND_SEQ_CLIENT_SYSTEM;
  sender.port = SND_SEQ_PORT_SYSTEM_ANNOUNCE;
  snd_seq_port_subscribe_set_sender(sub, &sender);
  report("subscribe port (system announce)",snd_seq_subscribe_port(S->handle, sub));

  sender.client = SND_SEQ_CLIENT_SYSTEM;
  sender.port = SND_SEQ_PORT_SYSTEM_TIMER;
  snd_seq_port_subscribe_set_sender(sub, &sender);
  report("subscribe port (system timer)",snd_seq_subscribe_port(S->handle, sub));
}

void init_seq(seq_data* S) {
  int err;
  open_client(S);
  // printf("handle %p\n",S->handle);
  // printf("client %d\n", S->client);
  create_queue(S);
}

void end_seq(seq_data* S) {
  snd_seq_stop_queue(S->handle,S->queue,NULL);
  snd_seq_free_queue(S->handle,S->queue);

  for( seq_port_list* p = S->ports; p; p = p->cdr ) {
    snd_seq_delete_port(S->handle,p->car->port);
  }
  snd_seq_close(S->handle);
}

/* -------------------- Exchange Protocol -------------- */

typedef struct buffer_reader {
  const uint8_t* base;
  size_t remain;
} buffer_reader;

uint32_t read32(buffer_reader* buf) { const uint32_t value = be32toh(* (const uint32_t*)buf->base); buf->base += 4; buf->remain -= 4; return value; }
uint16_t read16(buffer_reader* buf) { const uint16_t value = be16toh(* (const uint16_t*)buf->base); buf->base += 2; buf->remain -= 2; return value; }
uint8_t  read8 (buffer_reader* buf) { const uint8_t  value = * (const uint8_t* )buf->base; buf->base += 1; buf->remain -= 1; return value; }

enum { buffer_writer_len = 500 };
typedef struct buffer_writer {
  uint8_t data[buffer_writer_len];
  size_t pos;
} buffer_writer;

uint32_t* write32(buffer_writer* buf, uint32_t value) { uint32_t* addr = (uint32_t*)(buf->data+buf->pos); *addr = htobe32(value); buf->pos += 4; return addr; }
uint16_t* write16(buffer_writer* buf, uint16_t value) { uint16_t* addr = (uint16_t*)(buf->data+buf->pos); *addr = htobe16(value); buf->pos += 2; return addr; }
uint8_t*  write8 (buffer_writer* buf, uint8_t  value) { uint8_t * addr = (uint8_t *)(buf->data+buf->pos); *addr = value; buf->pos += 1; return addr; }
void writebuf(buffer_writer* buf, const char* str, ssize_t len) {
  if( len > buffer_writer_len-buf->pos-1 ) {
    len = buffer_writer_len-buf->pos-1;
  }
  if( len > 255 ) {
    len = 255;
  }
  write8(buf,len);
  memcpy(buf->data+buf->pos,str,len);
  buf->pos += len;
}
void writebufs(buffer_writer* buf, const char* str) {
  writebuf(buf,str,strlen(str));
}

// Send a UDP packet out.
void send_packet(app_data*,const buffer_writer*);

/* ## Parses a received UDP packet and send it out over MIDI. */

enum {
  VERSION_MASK = 0xf0,
  VERSION = 0x10,
  SCHEDULED_MASK = 0x01,
  REALTIME_MASK = 0x02,
  RELATIVE_MASK = 0x04,
};

void handle_SD_MSG_MIDI(seq_data *s,buffer_reader* buf, uint8_t first_byte) {

  if( buf->remain < 10 ) {
    fprintf(stderr,"! Message is too short (%d bytes)\n",buf->remain);
    return;
  }

  short scheduled = first_byte & SCHEDULED_MASK; // 0 if direct (no timestamp), 1 if scheduled
  short real      = first_byte & REALTIME_MASK;  // 0 if tick time, 1 if real time
  short relative  = first_byte & RELATIVE_MASK;  // 0 if absolute, 1 if relative

  snd_seq_timestamp_t timestamp;
  if (scheduled) {
    if (real) {
      timestamp.time.tv_sec  = read32(buf);
      timestamp.time.tv_nsec = read32(buf);
    } else {
      timestamp.tick = read32(buf);
      read32(buf); // ignore
    }
  } else {
    read32(buf); // ignore
    read32(buf); // ignore
  }

  if (buf->remain < 2) return;
  ssize_t midi_length = read16(buf);

  if(midi_length > buf->remain) {
    fprintf(stderr,"! Received message has midi_length=%d, remain=%d\n",midi_length,buf->remain);
    return;
  }

  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);

  if( midi_length == 0 ) {
    snd_midi_event_reset_encode(s->output_parser);
    fprintf(stderr,"+ handle_SD_MSG_MIDI reset encoder\n");
    return;
  }

  long parsed = snd_midi_event_encode(s->output_parser,buf->base,midi_length,&ev);
  buf->base += parsed; buf->remain -= parsed;

  /*
  ev.type = SND_SEQ_EVENT_USR_VAR0;
  snd_seq_ev_set_source(&ev,s->port);
  // snd_seq_ev_set_dest(&ev,dest_client,dest_port);
  // snd_seq_ev_set_broadcast(&ev); // broadcast to all (needs perms)
  */
  snd_seq_ev_set_subs(&ev); // broadcast to subscribers

  int err;
  if(!scheduled) { // send now: not queued
    snd_seq_ev_set_direct(&ev);
    // snd_seq_ev_set_variable(&ev,1,"abcdefgh"); // segfaults
    // snd_seq_ev_set_varusr(&ev,len,base);

    // Not sure which one is best. Both work.
    report("output direct", snd_seq_event_output_direct(s->handle,&ev));
    // report("output buffer",snd_seq_event_output_buffer(s->handle,&ev));
  } else { // send later
    // snd_seq_ev_set_variable(&ev,len,base); // segfaults
    if (real) {
      report("schedule real",snd_seq_ev_schedule_real(&ev,s->queue,relative,&timestamp.time));
    } else {
      report("schedule tick",snd_seq_ev_schedule_tick(&ev,s->queue,relative,timestamp.tick));
    }
    report("output buffer",snd_seq_event_output_buffer(s->handle,&ev));
  }
}

void handle_SD_MSG_COMMAND(seq_data *s,buffer_reader* buf, uint8_t first_byte);

#include <endian.h> // glibc-specific

typedef enum {
  SD_MSG_MARK = 0,  // MARK message
  SD_MSG_MIDI,      // MIDI message to parse and send out (UDP→ MIDI)
  SD_MSG_SEQ_EVENT, // seq_event (MIDI→UDP)
  SD_MSG_COMMAND    // commands (UDP→service)
} sd_msg_type_t;

/*
 * UDP packet to MIDI
 */

// We need to provide:
// - direct vs non-direct
// - tick vs real
// - absolute vs relative
// - one or two `unsigned int`s

void send_mark(seq_data* s);

void parse_one(seq_data* s, buffer_reader* buf) {

  if( buf->remain < 2 ) {
    fprintf(stderr,"! Message is too short (%d bytes)\n",buf->remain);
    return;
  }

  uint8_t first_byte = read8(buf);
  sd_msg_type_t sd_msg = read8(buf);

  switch (sd_msg) {
    case SD_MSG_MARK:
      send_mark(s);
      break;
    case SD_MSG_MIDI:
      handle_SD_MSG_MIDI(s,buf,first_byte);
      break;
    case SD_MSG_COMMAND:
      handle_SD_MSG_COMMAND(s,buf,first_byte);
      break;
    default:
      fprintf(stderr,"! Ignoring SD_MSG type %d\n",sd_msg);
      break;
  }
}

void send_mark(seq_data* s) {
  /* Provide realtime clock back */

  snd_seq_queue_status_t * status;
  snd_seq_queue_status_alloca(&status);
  report("queue status (send_mark)",snd_seq_get_queue_status(s->handle,s->queue,status));
  const snd_seq_real_time_t* real_time = snd_seq_queue_status_get_real_time(status);

  buffer_writer wbuf;
  wbuf.pos = 0;

  // First byte
  unsigned char first_byte = VERSION;
  first_byte |= SCHEDULED_MASK;
  first_byte |= REALTIME_MASK;
  write8(&wbuf,first_byte);
  write8(&wbuf,SD_MSG_MARK);
  write32(&wbuf,real_time->tv_sec);
  write32(&wbuf,real_time->tv_nsec);
  write16(&wbuf,0); // MIDI data length = 0
  send_packet(s->app,&wbuf);
}

void on_read_v1(seq_data* s, buffer_reader* buf) {
  parse_one(s,buf);
  report("drain output (on_read_v1)",snd_seq_drain_output(s->handle));
  send_mark(s);
}

/* ## MIDI event to UDP */

typedef enum {
  SD_TYPE_NONE = 0, // no data
  SD_TYPE_ANY, // unparsed
  SD_TYPE_RESULT,
  SD_TYPE_NOTE,
  SD_TYPE_CONTROL,
  SD_TYPE_QUEUE,
  SD_TYPE_ADDR,
  SD_TYPE_CONNECT,
  SD_TYPE_RAW8,
  SD_TYPE_EXT,
  SD_TYPE_TIME // unused?
} sd_type_t;

void encode_seq_event(buffer_writer* wbuf,seq_data* seq,snd_seq_event_t* ev) {

  wbuf->pos = 0;

  // First byte
  unsigned char first_byte = VERSION;
  first_byte |= SCHEDULED_MASK;
  if(ev->flags & SND_SEQ_TIME_STAMP_MASK == SND_SEQ_TIME_STAMP_REAL) {
    first_byte |= REALTIME_MASK;
  }
  if(ev->flags & SND_SEQ_TIME_MODE_MASK == SND_SEQ_TIME_MODE_REL) {
    first_byte |= RELATIVE_MASK;
  }
  write8(wbuf,first_byte);
  write8(wbuf,SD_MSG_SEQ_EVENT);

  // 8 bytes for timestamp
  if(ev->flags & SND_SEQ_TIME_STAMP_MASK == SND_SEQ_TIME_STAMP_REAL) {
    write32(wbuf,ev->time.time.tv_sec);
    write32(wbuf,ev->time.time.tv_nsec);
  } else {
    write32(wbuf,ev->time.tick);
    write32(wbuf,-1);
  }

  /* Ideally we should parse the event content and pass it along properly formatted. */

  sd_type_t sdtype = SD_TYPE_NONE;

  /* Based on snd_seq_event_type in alsa-lib/seq_event.h */
  if(ev->type < SND_SEQ_EVENT_NOTE) {
    sdtype = SD_TYPE_RESULT;
  } else if (ev->type < SND_SEQ_EVENT_CONTROLLER) {
    sdtype = SD_TYPE_NOTE;
  } else if (ev->type < SND_SEQ_EVENT_START) {
    sdtype = SD_TYPE_CONTROL;
  } else if (ev->type < SND_SEQ_EVENT_TUNE_REQUEST) {
    sdtype = SD_TYPE_QUEUE;
  } else if (ev->type < SND_SEQ_EVENT_ECHO) {
    sdtype = SD_TYPE_NONE;
  } else if (ev->type < SND_SEQ_EVENT_CLIENT_START) {
    sdtype = SD_TYPE_ANY;
  } else if (ev->type < SND_SEQ_EVENT_PORT_SUBSCRIBED) {
    sdtype = SD_TYPE_ADDR;
  } else if (ev->type < SND_SEQ_EVENT_USR0) {
    sdtype = SD_TYPE_CONNECT;
  } else if (ev->type < SND_SEQ_EVENT_SYSEX) {
    sdtype = SD_TYPE_RAW8;
  } else if (ev->type < SND_SEQ_EVENT_NONE) {
    sdtype = SD_TYPE_EXT;
  }

  /* Based on sdtype, add MIDI content if available */
  switch (sdtype) {
    case SD_TYPE_NOTE:
    case SD_TYPE_CONTROL:
    case SD_TYPE_RAW8:
      {
      /* FIXME estimate the length, write into a temp buffer, then write at once in wbuf */
      uint16_t* size_loc = write16(wbuf,0);

      snd_midi_event_reset_decode(seq->input_parser);
      long written = snd_midi_event_decode(seq->input_parser, wbuf->data+wbuf->pos, buffer_writer_len-wbuf->pos, ev);
      wbuf->pos += written;
      *size_loc = htobe16(written);
      }
      break;

    default:
      {
      write16(wbuf,0);
      }
      break;
  }

  /* event data */
  write8(wbuf,ev->type);
  write8(wbuf,ev->flags);
  write8(wbuf,ev->queue);
  write8(wbuf,ev->source.client);
  write8(wbuf,ev->source.port);
  write8(wbuf,ev->dest.client);
  write8(wbuf,ev->dest.port);

  /* event content data type */
  write8(wbuf,sdtype);

  switch(sdtype) {

    case SD_TYPE_NONE:
      write16(wbuf,0);
      break;

    case SD_TYPE_ANY:
      // TBD
      write16(wbuf,0);
      break;

    case SD_TYPE_RESULT:
      write16(wbuf,8);
      write32(wbuf,ev->data.result.event);
      write32(wbuf,ev->data.result.result);
      break;

    case SD_TYPE_NOTE:
      write16(wbuf,8);
      write8(wbuf,ev->data.note.channel);
      write8(wbuf,ev->data.note.note);
      write8(wbuf,ev->data.note.velocity);
      write8(wbuf,ev->data.note.off_velocity);
      write32(wbuf,ev->data.note.duration);
      break;

    case SD_TYPE_CONTROL:
      write16(wbuf,9);
      write8(wbuf,ev->data.control.channel);
      write32(wbuf,ev->data.control.param);
      write32(wbuf,ev->data.control.value);
      break;

    case SD_TYPE_QUEUE:
      write16(wbuf,0);
      // TBD
      // snd_seq_ev_queue_control_t content = ev->data.queue;
      break;

    case SD_TYPE_ADDR:
      write16(wbuf,2);
      write8(wbuf,ev->data.addr.client);
      write8(wbuf,ev->data.addr.port);
      break;

    case SD_TYPE_CONNECT:
      write16(wbuf,4);
      write8(wbuf,ev->data.connect.sender.client);
      write8(wbuf,ev->data.connect.sender.port);
      write8(wbuf,ev->data.connect.dest.client);
      write8(wbuf,ev->data.connect.dest.port);
      break;

    case SD_TYPE_RAW8:
      write16(wbuf,12);
      for ( ssize_t i = 0; i < 12; i++ ) {
        write8(wbuf,ev->data.raw8.d[i]);
      }
      break;

    case SD_TYPE_EXT:
      write16(wbuf,ev->data.ext.len);
      for ( ssize_t i = 0; i < ev->data.ext.len; i++ ) {
        write8(wbuf,((uint8_t*)ev->data.ext.ptr)[i]);
      }
      break;

    case SD_TYPE_TIME: // unused?
      write16(wbuf,0);
      break;

  }

}

void pull_midi_event(app_data* data,seq_data* seq) {
  snd_seq_event_t *ev;
  /* FIXME retry in case of failure */
  if (snd_seq_event_input(seq->handle, &ev)<0) {
    return;
  }
  if (!ev) {
    return;
  }

  buffer_writer wbuf;
  encode_seq_event(&wbuf,seq,ev);

  send_packet(data,&wbuf);

  snd_seq_free_event(ev);
}

// --------------- libuv portion -------------

#include <unistd.h>
#include <uv.h>

typedef enum {
  SD_CMD_HOSTNAME = 0
, SD_CMD_SYSNAME
, SD_CMD_LOCAL_CLIENT
, SD_CMD_ENUMERATE_CLIENTS
, SD_CMD_ENUMERATE_PORTS
, SD_CMD_ENUMERATE_SUBSCRIPTIONS
, SD_CMD_CONNECT
, SD_CMD_DISCONNECT
, SD_CMD_CREATE_PORT
, SD_CMD_CLOSE_PORT

, SD_REP_HOSTNAME = 128
, SD_REP_SYSNAME
, SD_REP_LOCAL_CLIENT
, SD_REP_CLIENT
, SD_REP_PORT
, SD_REP_SUBSCRIPTION
, SD_REP_CREATED_PORT
} sd_msg_command_t;

void handle_SD_MSG_COMMAND(seq_data *s,buffer_reader* buf, uint8_t first_byte) {
  app_data* data = s->app;
  uint8_t command = read8(buf);

  buffer_writer rep;
  rep.pos = 0;

  first_byte = VERSION;
  write8(&rep,first_byte);
  write8(&rep,SD_MSG_COMMAND);

  switch (command) {
    case SD_CMD_HOSTNAME:
      {
        write8(&rep,SD_REP_HOSTNAME);

        ssize_t written = buffer_writer_len-rep.pos;
        if(!uv_os_gethostname(rep.data+rep.pos,&written)) {
          rep.pos += written;
          send_packet(data,&rep);
        }
      }
      break;

    case SD_CMD_SYSNAME:
      {
        write8(&rep,SD_REP_SYSNAME);

        uv_utsname_t buf;
        if(!uv_os_uname(&buf)) {
          writebufs(&rep,buf.sysname);
          send_packet(data,&rep);
        }
      }
      break;

    case SD_CMD_LOCAL_CLIENT:
      {
        write8(&rep,SD_REP_LOCAL_CLIENT);
        write32(&rep,s->client);
        write32(&rep,s->max_clients);
        write32(&rep,s->max_ports);
        write32(&rep,s->max_queues);
        write32(&rep,s->max_channels);
        send_packet(data,&rep);
      }

    case SD_CMD_ENUMERATE_CLIENTS:
      {
        // Enumerate all clients
        int client_id = read32(buf);

        write8(&rep,SD_REP_CLIENT);

        snd_seq_client_info_t* info;
        snd_seq_client_info_alloca(&info);

        snd_seq_client_info_set_client(info,client_id);

        int outcome = snd_seq_query_next_client(s->handle, info);
        write32(&rep,outcome);
        write32(&rep,snd_seq_client_info_get_client(info));
        write32(&rep,snd_seq_client_info_get_type(info));
        writebufs(&rep,snd_seq_client_info_get_name(info));
        write32(&rep,snd_seq_client_info_get_num_ports(info));
        send_packet(data,&rep);
      }
      break;

    case SD_CMD_ENUMERATE_PORTS:
      {
        // Enumerate all ports
        int client_id = read32(buf);
        int port_id   = read32(buf);

        write8(&rep,SD_REP_PORT);

        snd_seq_port_info_t* pinfo;
        snd_seq_port_info_alloca(&pinfo);

        snd_seq_port_info_set_client(pinfo,client_id);
        snd_seq_port_info_set_port(pinfo,port_id);

        int outcome = snd_seq_query_next_port(s->handle,pinfo);
        write32(&rep,outcome);
        write32(&rep,snd_seq_port_info_get_client(pinfo));
        write32(&rep,snd_seq_port_info_get_port(pinfo));
        write32(&rep,snd_seq_port_info_get_type(pinfo));
        writebufs(&rep,snd_seq_port_info_get_name(pinfo));
        write32(&rep,snd_seq_port_info_get_capability(pinfo));
        write32(&rep,snd_seq_port_info_get_midi_channels(pinfo));
        send_packet(data,&rep);
      }
      break;

    case SD_CMD_ENUMERATE_SUBSCRIPTIONS:
      {
        // Enumerate all subscriptions
        int client_id = read32(buf);
        int port_id   = read32(buf);
        int index     = read32(buf);
        snd_seq_query_subs_type_t type = read8(buf);

        write8(&rep,SD_REP_SUBSCRIPTION);

        snd_seq_query_subscribe_t* sinfo;
        snd_seq_query_subscribe_alloca(&sinfo);

        snd_seq_query_subscribe_set_client(sinfo,client_id);
        snd_seq_query_subscribe_set_port(sinfo,port_id);
        snd_seq_query_subscribe_set_index(sinfo,index);
        snd_seq_query_subscribe_set_type(sinfo,type);

        int outcome = snd_seq_query_port_subscribers(s->handle,sinfo);

        const snd_seq_addr_t * addr = snd_seq_query_subscribe_get_addr(sinfo);

        const snd_seq_addr_t * root = snd_seq_query_subscribe_get_root(sinfo);

        write32(&rep,outcome);
        write32(&rep,snd_seq_query_subscribe_get_client(sinfo));
        write32(&rep,snd_seq_query_subscribe_get_port(sinfo));
        write32(&rep,snd_seq_query_subscribe_get_index(sinfo));
        write32(&rep,snd_seq_query_subscribe_get_type(sinfo));
        write32(&rep,addr->client);
        write32(&rep,addr->port);
        write32(&rep,root->client);
        write32(&rep,root->port);
        send_packet(data,&rep);
      }
      break;

    case SD_CMD_CONNECT:
      {
        snd_seq_addr_t sender, dest;
        snd_seq_port_subscribe_t *subs;
        sender.client = read32(buf);
        sender.port = read32(buf);
        dest.client = read32(buf);
        dest.port = read32(buf);
        snd_seq_port_subscribe_alloca(&subs);
        snd_seq_port_subscribe_set_sender(subs, &sender);
        snd_seq_port_subscribe_set_dest(subs, &dest);
        int outcome = snd_seq_subscribe_port(s->handle, subs);
        fprintf(stderr,"+ Connect %d/%d to %d/%d: %s\n",
            sender.client, sender.port,
            dest.client, dest.port,
            snd_strerror(outcome));
      }
      break;

    case SD_CMD_DISCONNECT:
      {
        snd_seq_addr_t sender, dest;
        snd_seq_port_subscribe_t *subs;
        sender.client = read32(buf);
        sender.port = read32(buf);
        dest.client = read32(buf);
        dest.port = read32(buf);
        snd_seq_port_subscribe_alloca(&subs);
        snd_seq_port_subscribe_set_sender(subs, &sender);
        snd_seq_port_subscribe_set_dest(subs, &dest);
        int outcome = snd_seq_unsubscribe_port(s->handle, subs);
        fprintf(stderr,"+ Connect %d/%d to %d/%d: %s\n",
            sender.client, sender.port,
            dest.client, dest.port,
            snd_strerror(outcome));
      }
      break;

    case SD_CMD_CREATE_PORT:
      {
        write8(&rep,SD_REP_CREATED_PORT);

        PortDirection direction = read8(buf);
        ssize_t len = read8(buf);
        char* name = alloca(len+1);
        memcpy(name,buf->base,len);
        name[len] = '\0';
        int port = new_port(s,name,direction);
        write32(&rep,port);
        send_packet(data,&rep);
      }
      break;

    case SD_CMD_CLOSE_PORT:
      {
        int port = read32(buf);
        snd_seq_delete_port(s->handle,port);
        // Remove from the linked list
        seq_port_list* prev = NULL;
        for( seq_port_list* p = s->ports; p; p = p->cdr ) {
          if(p->car->port == port) {
            if(!prev) {
              s->ports = p->cdr;
            } else {
              prev->cdr = p->cdr;
            }
          }
          prev = p;
        }
      }
      break;
  }
}

// See e.g. https://github.com/libuv/libuv/blob/v1.x/docs/code/udp-dhcp/main.c

typedef struct app_data {
  uv_poll_t *polls;
  seq_data seq;
  uv_udp_t udp_socket;
  char const* source_host;
  int source_port;
  char const* dest_host;
  int dest_port;
} app_data;

// On UDP read we send out to MIDI

/* Might need to call during idle as well */
void subscription_callback(uv_poll_t* handle, int status, int events) {
  app_data* data = handle->data;
  seq_data* seq = &data->seq;

  pull_midi_event(data,seq);
}

void start_subscriptions(uv_loop_t* loop) {
  app_data* data = loop->data;
  seq_data* seq = &data->seq;

  int max = snd_seq_poll_descriptors_count(seq->handle, POLLIN);
  struct pollfd *pfds;
  pfds = alloca(sizeof(*pfds) * max);
  report("poll descriptors",snd_seq_poll_descriptors(seq->handle, pfds, max, POLLIN));

  data->polls = malloc(sizeof(uv_poll_t) * max);
  for( int i = 0; i < max; i++ ) {
    uv_poll_t *handle = data->polls+i;
    handle->data = data;
    int err = uv_poll_init(loop,handle,(pfds+i)->fd);
    uv_poll_start(handle,UV_READABLE,subscription_callback);
  }
}

void stop_subscriptions(uv_loop_t* loop) {
  app_data* data = loop->data;
  int max = snd_seq_poll_descriptors_count(data->seq.handle, POLLIN);
  for( int i = 0; i++; i < max ) {
    uv_poll_t *handle = data->polls+i;
    uv_poll_stop(handle);
  }
}

void on_read(uv_udp_t* udp_socket, ssize_t nread, const uv_buf_t* buf, const struct sockaddr* addr, unsigned flags){
  app_data* data = udp_socket->data;
  seq_data* seq = &data->seq;

  // Only handle content with at least one byte description and one byte content.
  if(nread >= 2) {
    /* There's some parsing to be done here so that multiple MIDI events can be packed into a single UDP datagram. */

    /* Top nibble of first byte is version. */
    buffer_reader reader;
    reader.base = (const uint8_t*)buf->base;
    reader.remain = nread;

    switch(buf->base[0]&VERSION_MASK) {
      case VERSION:
        on_read_v1(seq,&reader);
        break;
      default:
        fprintf(stderr,"! Received unknown Version (in %#02x)\n",buf->base[0]);
        fprintf(stderr,"! Len = %d\n!",buf->len);
        for( int i = 0; i < buf->len; i++ ) {
          fprintf(stderr," %02x",buf->base[i]);
        }
        fprintf(stderr,"\n");
        break;
    }
  }

  // Clear the buffer we created in alloc_buffer()
  if( flags & UV_UDP_MMSG_CHUNK ) {
    fprintf(stderr,"+ (on_read) not freeing %p %d\n", buf->base, buf->len);
  } else {
    free(buf->base);
  }
}

void alloc_buffer(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf) {
  buf->base = malloc(suggested_size);
  buf->len = suggested_size;
}

void init_socket(uv_loop_t* loop) {
  app_data* data = loop->data;
  uv_udp_init(loop, &data->udp_socket);
  struct sockaddr_in6 addr;
  uv_ip6_addr(data->source_host, data->source_port, &addr);
  uv_udp_bind(&data->udp_socket, (const struct sockaddr *)&addr, UV_UDP_REUSEADDR);
  data->udp_socket.data = data;
  uv_udp_recv_start(&data->udp_socket, alloc_buffer, on_read);
}

// Invoked on successful / unsuccessul UDP send
void on_sent(uv_udp_send_t* req, int status) {
  // Clear the buffer we created inside send_packet()
  free(req->data);
  free(req);
}

void send_packet(app_data* data,const buffer_writer* buf) {
  uv_buf_t msg;
  msg.len  = buf->pos;
  msg.base = malloc(msg.len);
  memcpy(msg.base,buf->data,msg.len);

  uv_udp_send_t* send_req;
  send_req = malloc(sizeof(uv_udp_send_t));
  send_req->data = msg.base;

  struct sockaddr_in6 addr;
  uv_ip6_addr(data->dest_host, data->dest_port, &addr);
  uv_udp_send(send_req,&data->udp_socket,&msg,1,(const struct sockaddr*)&addr,on_sent);
}

#define DEFAULT_SOURCE_HOST "::"
#define DEFAULT_SOURCE_PORT 3031
#define DEFAULT_DEST_HOST "::1"
#define DEFAULT_DEST_PORT 3032

#define ENV_SOURCE_HOST "SOURCE_HOST"
#define ENV_SOURCE_PORT "SOURCE_PORT"
#define ENV_DEST_HOST "DEST_HOST"
#define ENV_DEST_PORT "DEST_PORT"

int main(int argc,char* argv[]) {
  uv_loop_t *loop;
  loop = uv_default_loop();

  app_data data;

  {
  data.source_host = getenv(ENV_SOURCE_HOST);
  if(!data.source_host) { data.source_host = DEFAULT_SOURCE_HOST; }
  char * str = getenv(ENV_SOURCE_PORT);
  data.source_port = 0;
  if(str) { data.source_port = atoi(str); }
  if(!data.source_port) { data.source_port = DEFAULT_SOURCE_PORT; }
  printf("+ Using local source %s:%d\n", data.source_host, data.source_port);
  }

  {
  data.dest_host = getenv(ENV_DEST_HOST);
  if(!data.dest_host) { data.dest_host = DEFAULT_DEST_HOST; }
  char * str = getenv(ENV_DEST_PORT);
  data.dest_port = 0;
  if(str) { data.dest_port = atoi(str); }
  if(!data.dest_port) { data.dest_port = DEFAULT_DEST_PORT; }
  printf("+ Using remote destination %s:%d\n", data.dest_host, data.dest_port);
  }

  seq_data seq;
  data.seq.app = &data;
  loop->data = &data;
  init_seq(&data.seq);


  init_socket(loop);
  start_subscriptions(loop);

  uv_run(loop,UV_RUN_DEFAULT);

  uv_udp_recv_stop(&data.udp_socket);
  uv_loop_close(loop);
  end_seq(&data.seq);
  stop_subscriptions(loop);
  return 0;
}

// Manage subscriptions:
// file:///usr/share/doc/libasound2/html/group___seq_subscribe.html
// and example under "Arbitrary connection" in file:///usr/share/doc/libasound2/html/seq.html
// - snd_seq_port_subscribe_alloca()
// - snd_seq_subscribe_port
// - snd_seq_unsubscribe_port
// - snd_seq_get_port_subscription
