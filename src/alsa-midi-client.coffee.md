    dgram = require 'dgram'
    child = require 'child_process'
    EventEmitter = require 'eventemitter3'
    assert = require 'assert'
    {join} = require 'path'

    integer = (t) ->
      v = parseInt t
      if isNaN v
        null
      else
        v

    Catch = (f) ->
      (args...) ->
        try
          await f args...
        catch error
          console.error error

    SND_SEQ_EVENT = require './snd_seq_events.cjs'
    SND_SEQ_EVENT_names = {}
    do ->
      for own k, v of SND_SEQ_EVENT
        SND_SEQ_EVENT_names[v] = k

    VERSION = 0x10
    SCHEDULED = 0x01
    REALTIME = 0x02
    RELATIVE = 0x04
    SD_MSG_MARK = 0
    SD_MSG_MIDI = 1
    SD_MSG_SEQ_EVENT = 2
    SD_MSG_COMMAND = 3

    SD_CMD_HOSTNAME = 0
    SD_CMD_SYSNAME = 1
    SD_CMD_LOCAL_CLIENT = 2
    SD_CMD_ENUMERATE_CLIENTS = 3
    SD_CMD_ENUMERATE_PORTS = 4
    SD_CMD_ENUMERATE_SUBSCRIPTIONS = 5
    SD_CMD_CONNECT = 6
    SD_CMD_DISCONNECT = 7
    SD_CMD_CREATE_PORT = 8
    SD_CMD_DELETE_PORT = 9

    SD_REP_HOSTNAME = 128
    SD_REP_SYSNAME = 129
    SD_REP_LOCAL_CLIENT = 130
    SD_REP_CLIENT = 131
    SD_REP_PORT = 132
    SD_REP_SUBSCRIPTION = 133
    SD_REP_CREATED_PORT = 134

    SD_TYPE_NONE = 0
    SD_TYPE_ANY = 1 # unparsed
    SD_TYPE_RESULT = 2
    SD_TYPE_NOTE = 3
    SD_TYPE_CONTROL = 4
    SD_TYPE_QUEUE = 5
    SD_TYPE_ADDR = 6
    SD_TYPE_CONNECT = 7
    SD_TYPE_RAW8 = 8
    SD_TYPE_EXT = 9
    SD_TYPE_TIME = 10 # unused?

    class AlsaMIDIClient extends EventEmitter
      constructor: (name) ->
        do super

        @clients = new Map
        @ports = new Map

        @on 'CLIENT_START', ({addr_client}) ->
          @get_client addr_client
        @on 'CLIENT_CHANGE', ({addr_client}) ->
          @get_client addr_client
        @on 'CLIENT_EXIT', ({addr_client}) ->
          @clients.delete addr_client

        @on 'PORT_START', ({addr_client,addr_port}) ->
          @get_port addr_client, addr_port
        @on 'PORT_CHANGE', ({addr_client,addr_port}) ->
          @get_port addr_client, addr_port
        @on 'PORT_EXIT', ({addr_client,addr_port}) ->
          @ports.delete [addr_client,addr_port]

        env =
          NAME: name
        @seq = child.spawn (join __dirname, '../bin/alsa-midi-gateway'), [],
          stdio: 'inherit'
          env: env

        @seq.on 'error', (err) ->
          console.error "AlsaMIDIClient child", err
          return

        @server = dgram.createSocket 'udp6'
        @server.on 'error', (err) ->
          console.error "AlsaMIDIClient UDP", err
          # server.close()
          return

        @tv_sec = 0
        @tv_nsec = 0

        @init_sent = false
        @server.on 'message', Catch (msg,rinfo) =>
          # console.log "received: #{msg.toString('hex')} from #{rinfo.address}:#{rinfo.port}"
          if not @__init_sent
            @initialize()
            @__init_sent = true
          @parseMessage msg, rinfo
          return

        @server.on 'listening', =>
          address = @server.address()
          console.log "AlsaMIDIClient: UDP server listening #{address.address}:#{address.port}"
          return

        @server.on 'connect', ->
          console.log 'AlsaMIDIClient: UDP connected'
          return

        timer = setInterval =>
          @send Buffer.from [
            VERSION
            SD_MSG_MARK
          ]
          return
        , 1000

        dest_port = integer(process.env.DEST_PORT) ? 3032
        dest_host = process.env.DEST_HOST ? '::1'
        source_port = integer(process.env.SOURCE_PORT) ? 3031
        source_host = process.env.SOURCE_HOST ? '::1'

        @server.bind dest_port, dest_host
        @server.connect source_port, source_host

        @close = =>
          clearInterval timer
          @seq.kill()
          @server.close()
          return
        return

      send: (msg) ->
        assert msg?
        # console.log "Sending #{msg.toJSON().data}"
        @server.send msg
        return

      portConnect: (sender_client,sender_port,dest_client,dest_port) ->
        msg = Buffer.alloc 3+4*4
        pos = 0
        msg.writeUInt8 VERSION, pos++
        msg.writeUInt8 SD_MSG_COMMAND, pos++
        msg.writeUInt8 SD_CMD_CONNECT, pos++
        msg.writeInt32BE sender_client, pos; pos += 4
        msg.writeInt32BE sender_port, pos; pos += 4
        msg.writeInt32BE dest_client, pos; pos += 4
        msg.writeInt32BE dest_port, pos; pos += 4
        @send msg
        return

      portDisconnect: (sender_client,sender_port,dest_client,dest_port) ->
        msg = Buffer.alloc 3+4*4
        pos = 0
        msg.writeUInt8 VERSION, pos++
        msg.writeUInt8 SD_MSG_COMMAND, pos++
        msg.writeUInt8 SD_CMD_DISCONNECT, pos++
        msg.writeInt32BE sender_client, pos; pos += 4
        msg.writeInt32BE sender_port, pos; pos += 4
        msg.writeInt32BE dest_client, pos; pos += 4
        msg.writeInt32BE dest_port, pos; pos += 4
        @send msg
        return

      sendMidi: (midi_data) ->
        assert typeof midi_data is 'object'
        assert midi_data.length?
        msg = Buffer.from [
          VERSION
          SD_MSG_MIDI
          0,0,0,0 # timestamp (ignored)
          0,0,0,0
          0, 0 # length
          midi_data...
        ]
        msg.writeUInt16BE midi_data.length, 10
        @send msg
        return

      clientId: ->
        return @__local_client if @__local_client?
        timer = null
        p = new Promise (resolve) =>
          @once 'local-client', (local_client) ->
            clearInterval timer
            resolve local_client
          return
        timer = setInterval =>
          # console.log 'Sending CMD LOCAL CLIENT'
          @send Buffer.from [
            VERSION
            SD_MSG_COMMAND
            SD_CMD_LOCAL_CLIENT
          ]
        , 200
        await p

      createPort: (name,direction = 'both') ->
        assert typeof name is 'string'
        assert typeof direction is 'string'
        local_client = await @clientId()
        # console.log "Creating port #{name} on client #{local_client}"
        port_name = Buffer
          .from name
          .toJSON()
          .data
        p = new Promise (resolve,reject) =>
          success = (port) ->
            clearTimeout timer
            resolve port
            return
          failure = =>
            @off 'created-port', success
            reject new Error 'Timeout'
            return

          @on 'created-port', success
          timer = setTimeout failure, 1000
          return

        dir = switch direction
          when 'input' # input-only
            1
          when 'output' # output-only
            2
          else # input and output
            3

        @send Buffer.from [
          VERSION
          SD_MSG_COMMAND
          SD_CMD_CREATE_PORT
          dir
          port_name.length
          port_name...
        ]
        await p

      deletePort: (port) ->
        @send Buffer.from [
          VERSION
          SD_MSG_COMMAND
          SD_CMD_DELETE_PORT
          port
        ]

      initialize: ->

        await @clientId()
        # Regular startup
        @send Buffer.from [
          VERSION
          SD_MSG_COMMAND
          SD_CMD_HOSTNAME
        ]
        @send Buffer.from [
          VERSION
          SD_MSG_COMMAND
          SD_CMD_SYSNAME
        ]

        for await client from @enumerate_clients()
          # console.log "Querying client #{client.name}"
          for await port from @enumerate_ports client.client
            # console.log "Querying port #{client.name}/#{port.name}"
            for await sub from @enumerate_subscriptions client.client, port.port, 0 # READ
              sub # unused
              yes
            for await sub from @enumerate_subscriptions client.client, port.port, 1 # WRITE
              sub # unused
              yes

        console.log 'Ready'
        @emit 'ready'
        return

      parseMessage: (msg,rinfo) ->
        rinfo # use for linter, might be needed later

        pos = 0
        first_byte = msg.readUInt8 pos++
        sdmsg = msg.readUInt8 pos++

        switch sdmsg

Record current realtime clock (as received from sequencer.c).

          when SD_MSG_MARK

            t1 = msg.readUInt32BE pos; pos += 4
            t2 = msg.readUInt32BE pos; pos += 4

            if first_byte is (VERSION|SCHEDULED|REALTIME)
              @tv_sec = t1
              @tv_nsec = t2

Parse generic ALSA MIDI Sequencer Event

          when SD_MSG_SEQ_EVENT

            sd_parsed = {}

            t1 = msg.readUInt32BE pos; pos += 4
            t2 = msg.readUInt32BE pos; pos += 4

            sd_parsed.time_is_relative =
              if first_byte & RELATIVE
                true
              else
                false

            if first_byte & REALTIME
              sd_parsed.time = 'realtime'
              sd_parsed.tv_sec = t1
              sd_parsed.tv_nsec = t2
            else
              sd_parsed.time = 'ticks'
              sd_parsed.ticks = t1

            midi_length = msg.readUInt16BE  pos; pos += 2
            midi_data = msg.subarray pos, pos+midi_length; pos += midi_length

            sd_parsed.ev_type = msg.readUInt8 pos; pos += 1
            sd_parsed.ev_name = SND_SEQ_EVENT_names[sd_parsed.ev_type]

            sd_parsed.ev_flags = msg.readUInt8 pos; pos += 1
            sd_parsed.ev_queue = msg.readUInt8 pos; pos += 1
            sd_parsed.ev_source_client = msg.readUInt8 pos; pos += 1
            sd_parsed.ev_source_port = msg.readUInt8 pos; pos += 1
            sd_parsed.ev_dest_client = msg.readUInt8 pos; pos += 1
            sd_parsed.ev_dest_port = msg.readUInt8 pos; pos += 1

            sd_parsed.ev_sdtype = msg.readUInt8 pos; pos += 1

            ev_sdlen = msg.readUInt16BE pos; pos += 2
            sd_data = msg.subarray pos, pos+ev_sdlen; pos += ev_sdlen

            dpos = 0
            switch sd_parsed.ev_sdtype
              when SD_TYPE_NONE
                sd_parsed.type = 'none'
              when SD_TYPE_ANY
                sd_parsed.type = 'any'
              when SD_TYPE_RESULT
                sd_parsed.type = 'result'
                sd_parsed.result_event = sd_data.readUInt32BE dpos; dpos += 4
                sd_parsed.result_result = sd_data.readUInt32BE dpos; dpos += 4
              when SD_TYPE_NOTE
                sd_parsed.type = 'note'
                sd_parsed.note_channel = sd_data.readUInt8 dpos; dpos += 1
                sd_parsed.note_note = sd_data.readUInt8 dpos; dpos += 1
                sd_parsed.note_velocity = sd_data.readUInt8 dpos; dpos += 1
                sd_parsed.note_off_velocity = sd_data.readUInt8 dpos; dpos += 1
              when SD_TYPE_CONTROL
                sd_parsed.type = 'control'
                sd_parsed.control_channel = sd_data.readUInt8 dpos; dpos += 1
                sd_parsed.control_param = sd_data.readUInt32BE dpos; dpos += 4
                sd_parsed.control_value = sd_data.readUInt32BE dpos; dpos += 4
              when SD_TYPE_QUEUE
                sd_parsed.type = 'queue'
              when SD_TYPE_ADDR
                sd_parsed.type = 'addr'
                sd_parsed.addr_client = sd_data.readUInt8 dpos; dpos += 1
                sd_parsed.addr_port = sd_data.readUInt8 dpos; dpos += 1
              when SD_TYPE_CONNECT
                sd_parsed.type = 'connect'
                sd_parsed.sender_client = sd_data.readUInt8 dpos; dpos += 1
                sd_parsed.sender_port = sd_data.readUInt8 dpos; dpos += 1
                sd_parsed.dest_client = sd_data.readUInt8 dpos; dpos += 1
                sd_parsed.dest_port = sd_data.readUInt8 dpos; dpos += 1
              when SD_TYPE_RAW8
                sd_parsed.type = 'raw8'
                raw8_len = sd_data.readUInt16BE dpos; dpos += 2
                sd_parsed.raw8_data = sd_data.subarrays dpos, dpos+raw8_len; pos += raw8_len
              when SD_TYPE_EXT
                sd_parsed.type = 'ext'
                ext_len = sd_data.readUInt16BE dpos; dpos += 2
                sd_parsed.ext_data = sd_data.subarrays dpos, dpos+ext_len; pos += ext_len
              when SD_TYPE_TIME
                sd_parsed.type = 'time'
              else
                console.error "AlsaMIDIClient: Unknown SD_TYPE #{sd_parsed.ev_sdtype}"

            if midi_length > 0
              @emit 'midi', midi_data

            @emit 'seq_event', sd_parsed
            @emit sd_parsed.ev_name, sd_parsed

          when SD_MSG_COMMAND
            sdcmd = msg.readUInt8 pos++
            switch sdcmd

              when SD_REP_HOSTNAME
                hostname = msg.slice(3).toString()
                @__hostname = hostname
                @emit 'hostname', hostname

              when SD_REP_SYSNAME
                sysname = msg.slice(3).toString()
                @__sysname = sysname
                @emit 'sysname', sysname

              when SD_REP_LOCAL_CLIENT
                local_client = msg.readInt32BE pos; pos += 4
                @__local_client = local_client
                @emit 'local-client', local_client

              when SD_REP_CLIENT
                outcome = msg.readInt32BE pos; pos += 4
                client  = msg.readInt32BE pos; pos += 4
                type    = msg.readInt32BE pos; pos += 4
                name_len = msg.readUInt8 pos++
                name = msg.toString 'utf8',pos,pos+name_len; pos += name_len
                num_ports = msg.readInt32BE pos; pos += 4

                data = null
                if outcome
                  client++
                else
                  data = {
                    client
                    type
                    name
                    num_ports
                  }
                  @clients.set client, data

                @emit 'rep-client', data, {client}

              when SD_REP_PORT
                outcome = msg.readInt32BE pos; pos += 4
                client = msg.readInt32BE pos; pos += 4
                port = msg.readInt32BE pos; pos += 4
                type = msg.readInt32BE pos; pos += 4
                name_len = msg.readUInt8 pos++
                name = msg.toString 'utf8', pos, pos+name_len; pos += name_len
                capability = msg.readInt32BE pos; pos += 4
                midi_channels = msg.readInt32BE pos; pos += 4

                data = null
                if outcome
                  port++
                else
                  data = {
                    client
                    port
                    type
                    name
                    capability
                    midi_channels
                  }
                  @ports.set [client,port], data

                @emit 'rep-port', data, {client, port}

              when SD_REP_SUBSCRIPTION
                outcome = msg.readInt32BE pos; pos += 4
                client= msg.readInt32BE pos; pos += 4
                port= msg.readInt32BE pos; pos += 4
                index= msg.readInt32BE pos; pos += 4
                type= msg.readInt32BE pos; pos += 4
                addr_client= msg.readInt32BE pos; pos += 4
                addr_port= msg.readInt32BE pos; pos += 4
                root_client= msg.readInt32BE pos; pos += 4
                root_port= msg.readInt32BE pos; pos += 4

                data = null
                unless outcome
                  data = {
                    client
                    port
                    type
                    index
                    addr_client
                    addr_port
                    root_client
                    root_port
                  }

                @emit 'rep-subscription', data, {client, port, type, index}

              when SD_REP_CREATED_PORT
                port = msg.readInt32BE pos; pos += 4
                @emit 'created-port', port

        return

      sendAsync: (req,msg) ->
        p = new Promise (resolve) =>
          @once msg, resolve
        @send req
        p

      get_client: (client=0) ->
        # console.log 'Get client', client
        req = Buffer.alloc 3+4
        req.writeUInt8 VERSION, 0
        req.writeUInt8 SD_MSG_COMMAND,1
        req.writeUInt8 SD_CMD_ENUMERATE_CLIENTS, 2
        req.writeInt32BE client-1, 3
        @sendAsync req, 'rep-client'

      get_port: (client,port=0) ->
        # console.log 'Get port', client, port
        req = Buffer.alloc 3+8
        req.writeUInt8 VERSION, 0
        req.writeUInt8 SD_MSG_COMMAND,1
        req.writeUInt8 SD_CMD_ENUMERATE_PORTS, 2
        req.writeInt32BE client, 3
        req.writeInt32BE port-1, 7
        @sendAsync req, 'rep-port'

      get_subscription: (client,port,type,index=0) ->
        # console.log 'Get subscription', client, port, type, index
        req = Buffer.alloc 3+3*4+1
        req.writeUInt8 VERSION, 0
        req.writeUInt8 SD_MSG_COMMAND,1
        req.writeUInt8 SD_CMD_ENUMERATE_SUBSCRIPTIONS, 2
        req.writeInt32BE client, 3
        req.writeInt32BE port, 7
        req.writeInt32BE index, 11
        req.writeUInt8 type, 15
        @sendAsync req, 'rep-subscription'

      enumerate_clients: ->
        client = -1
        while data = await @get_client client+1
          yield data
          {client} = data
        return

      enumerate_ports: (client) ->
        port = -1
        while data = await @get_port client, port+1
          yield data
          {port} = data
        return

      enumerate_subscriptions: (client,port,type) ->
        index = -1
        while data = await @get_subscription client, port, type, index
          yield data
          {index} = data
        return

      module.exports = AlsaMIDIClient
