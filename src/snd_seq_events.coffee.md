    SND_SEQ_EVENT =
      # system status; event data type: #snd_seq_result_t
      SYSTEM: 0
      # returned result status; event data type: #snd_seq_result_t
      RESULT: 1

      # note on and off with duration; event data type: #snd_seq_ev_note_t
      NOTE: 5,
      # note on; event data type: #snd_seq_ev_note_t
      NOTEON: 6
      # note off; event data type: #snd_seq_ev_note_t
      NOTEOFF: 7
      # key pressure change (aftertouch); event data type: #snd_seq_ev_note_t
      KEYPRESS: 8

      # controller; event data type: #snd_seq_ev_ctrl_t
      CONTROLLER: 10,
      # program change; event data type: #snd_seq_ev_ctrl_t
      PGMCHANGE: 11
      # channel pressure; event data type: #snd_seq_ev_ctrl_t
      CHANPRESS: 12
      # pitchwheel; event data type: #snd_seq_ev_ctrl_t; data is from -8192 to 8191)
      PITCHBEND: 13
      # 14 bit controller value; event data type: #snd_seq_ev_ctrl_t
      CONTROL14: 14
      # 14 bit NRPN;  event data type: #snd_seq_ev_ctrl_t
      NONREGPARAM:15
      # 14 bit RPN; event data type: #snd_seq_ev_ctrl_t
      REGPARAM: 16

      # SPP with LSB and MSB values; event data type: #snd_seq_ev_ctrl_t
      SONGPOS: 20
      # Song Select with song ID number; event data type: #snd_seq_ev_ctrl_t
      SONGSEL: 21
      # midi time code quarter frame; event data type: #snd_seq_ev_ctrl_t
      QFRAME: 22
      # SMF Time Signature event; event data type: #snd_seq_ev_ctrl_t
      TIMESIGN: 23
      # SMF Key Signature event; event data type: #snd_seq_ev_ctrl_t
      KEYSIGN: 24

      # MIDI Real Time Start message; event data type: #snd_seq_ev_queue_control_t
      START: 30
      # MIDI Real Time Continue message; event data type: #snd_seq_ev_queue_control_t
      CONTINUE: 31
      # MIDI Real Time Stop message; event data type: #snd_seq_ev_queue_control_t
      STOP: 32
      # Set tick queue position; event data type: #snd_seq_ev_queue_control_t
      SETPOS_TICK: 33
      # Set real-time queue position; event data type: #snd_seq_ev_queue_control_t
      SETPOS_TIME: 34
      # (SMF) Tempo event; event data type: #snd_seq_ev_queue_control_t
      TEMPO: 35
      # MIDI Real Time Clock message; event data type: #snd_seq_ev_queue_control_t
      CLOCK: 36
      # MIDI Real Time Tick message; event data type: #snd_seq_ev_queue_control_t
      TICK: 37
      # Queue timer skew; event data type: #snd_seq_ev_queue_control_t
      QUEUE_SKEW: 38
      # Sync position changed; event data type: #snd_seq_ev_queue_control_t
      SYNC_POS: 39

      # Tune request; event data type: none
      TUNE_REQUEST: 40
      # Reset to power-on state; event data type: none
      RESET: 41
      # Active sensing event; event data type: none
      SENSING: 42

      # Echo-back event; event data type: any type
      ECHO: 50
      # OSS emulation raw event; event data type: any type
      OSS: 51

      # New client has connected; event data type: #snd_seq_addr_t
      CLIENT_START: 60
      # Client has left the system; event data type: #snd_seq_addr_t
      CLIENT_EXIT: 61
      # Client status/info has changed; event data type: #snd_seq_addr_t
      CLIENT_CHANGE: 62
      # New port was created; event data type: #snd_seq_addr_t
      PORT_START: 63
      # Port was deleted from system; event data type: #snd_seq_addr_t
      PORT_EXIT: 64
      # Port status/info has changed; event data type: #snd_seq_addr_t
      PORT_CHANGE: 65

      # Ports connected; event data type: #snd_seq_connect_t
      PORT_SUBSCRIBED: 66
      # Ports disconnected; event data type: #snd_seq_connect_t
      PORT_UNSUBSCRIBED: 67

      # user-defined event; event data type: any (fixed size)
      USR0: 90
      # user-defined event; event data type: any (fixed size)
      USR1: 91
      # user-defined event; event data type: any (fixed size)
      USR2: 92
      # user-defined event; event data type: any (fixed size)
      USR3: 93
      # user-defined event; event data type: any (fixed size)
      USR4: 94
      # user-defined event; event data type: any (fixed size)
      USR5: 95
      # user-defined event; event data type: any (fixed size)
      USR6: 96
      # user-defined event; event data type: any (fixed size)
      USR7: 97
      # user-defined event; event data type: any (fixed size)
      USR8: 98
      # user-defined event; event data type: any (fixed size)
      USR9: 99

      # system exclusive data (variable length);  event data type: #snd_seq_ev_ext_t
      SYSEX: 130
      # error event;  event data type: #snd_seq_ev_ext_t
      BOUNCE: 131
      # reserved for user apps;  event data type: #snd_seq_ev_ext_t
      USR_VAR0: 135
      # reserved for user apps; event data type: #snd_seq_ev_ext_t
      USR_VAR1: 136
      # reserved for user apps; event data type: #snd_seq_ev_ext_t
      USR_VAR2: 137
      # reserved for user apps; event data type: #snd_seq_ev_ext_t
      USR_VAR3: 138
      # reserved for user apps; event data type: #snd_seq_ev_ext_t
      USR_VAR4: 139

      # NOP; ignored in any case
      NONE: 255

    module.exports = SND_SEQ_EVENT
