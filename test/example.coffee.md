    {AlsaMidiClient} = require '..'
    midi = new AlsaMidiClient 'Good doggy'

    velocity = 0

    Catch = (name,fn) ->
      (args...) ->
        try
          fn args...
        catch error
          console.error name, error

    midi.on 'ready', Catch 'example on ready', ->
        console.log 'Clients', midi.clients
        console.log 'Ports', midi.ports
        # Demo
        midi.portConnect 14, 0, 14, 0
        console.log 'example: createPort'
        port = await midi.createPort 'MIDI'
        console.log "Created port #{port}"

        console.log 'example: sendMidi'
        midi.sendMidi [
          0x91, 0x40, velocity
        ]

        console.log 'example: deletePort'
        midi.deletePort port
        console.log 'example: done'
        ###
        # Send one "DIRECT" message
        midi.send Buffer.from [
          midi.VERSION
          midi.SD_MSG_MIDI
          0,0,0,0 # timestamp (ignored)
          0,0,0,0
          0,3     # MIDI data length
          0x91, 0x40, velocity
        ]

        buf = Buffer.alloc 12+3
        pos = buf.writeUInt8 midi.VERSION|midi.SCHEDULED|midi.REALTIME, 0
        pos = buf.writeUInt8 SD_MSG_MIDI, pos
        pos = buf.writeUInt32BE tv_sec+2, pos
        pos = buf.writeUInt32BE tv_nsec+500000000, pos
        pos = buf.writeUInt16BE 3, pos
        pos = buf.writeUInt8 0x90, pos
        pos = buf.writeUInt8 0x40, pos
        pos = buf.writeUInt8 velocity, pos
        midi.send buf

        velocity = (velocity+1) & 0x7f
        ###
        midi.close()
        return

    midi.on 'midi', (midi_data) ->
      # Handle MIDI, here's an example
      new_msg = Buffer.alloc 12+midi_data.length
      new_msg.writeUInt8 midi.VERSION, 0
      new_msg.writeUInt8 midi.SD_MSG_MIDI, 1
      new_msg.writeUInt16BE midi_data.length, 10
      midi_data.copy new_msg, 12
      midi.send new_msg
      code = midi_data.readUInt8 1
      for offset in [8,1,-1,16,-16,2,-2,32,-32] when 0 <= code+offset <= 254
        new_msg.writeUInt8 code+offset, 13
        midi.send new_msg
      return

    midi.on 'PORT_SUBSCRIBED', (data) ->
      console.log 'Port Subscribed', data

    midi.on 'PORT_UNSUBSCRIBED', (data) ->
      console.log 'Port Unsubscribed', data
